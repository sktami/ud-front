/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    fontFamily:{
      sans: ['PlusJakartaSans-Regular', "sans-serif"],
      serif: ["Zodiak-Regular", "serif"],
    },
    extend: {
      colors: {
        "ud-white": "#ffffff",
        "ud-zinc": "#f4f4f5",
        "ud-black": "#000000",
        "ud-green": '#006a4e',
        "ud-yellow": "#ffdb58",
      },
      fontFamily: {
        // plus-jakarta
        JakartaVariable: ['PlusJakartaSans-Variable', "sans-serif"],
        JakartaVariableItalic: ['PlusJakartaSans-VariableItalic', "sans-serif"],
        JakartaExtraLight: ['PlusJakartaSans-ExtraLight', "sans-serif"],
        JakartaExtraLightItalic: ['PlusJakartaSans-ExtraLightItalic', "sans-serif"],
        JakartaLight: ['PlusJakartaSans-Light', "sans-serif"],
        JakartaLightItalic: ['PlusJakartaSans-LightItalic', "sans-serif"],
        JakartaRegular: ['PlusJakartaSans-Regular', "sans-serif"],
        JakartaItalic: ['PlusJakartaSans-Italic', "sans-serif"],
        JakartaMedium: ['PlusJakartaSans-Medium', "sans-serif"],
        JakartaMediumItalic: ['PlusJakartaSans-MediumItalic', "sans-serif"],
        JakartaSemiBold: ['PlusJakartaSans-SemiBold', "sans-serif"],
        JakartaSemiBoldItalic: ['PlusJakartaSans-SemiBoldItalic', "sans-serif"],
        JakartaBold: ['PlusJakartaSans-Bold', "sans-serif"],
        JakartaBoldItalic: ['PlusJakartaSans-BoldItalic', "sans-serif"],
        JakartaExtraBold: ['PlusJakartaSans-ExtraBold', "sans-serif"],
        JakartaExtraBoldItalic: ['PlusJakartaSans-ExtraBoldItalic', "sans-serif"],
        // zodiak
        ZodiakVariable: ["Zodiak-Variable", "serif"],
        ZodiakVariableItalic: ["Zodiak-VariableItalic", "serif"],
        ZodiakThin: ["Zodiak-Thin", "serif"],
        ZodiakThinItalic: ["Zodiak-ThinItalic", "serif"],
        ZodiakLight: ["Zodiak-Light", "serif"],
        ZodiakLightItalic: ["Zodiak-LightItalic", "serif"],
        ZodiakRegular: ["Zodiak-Regular", "serif"],
        ZodiakItalic: ["Zodiak-Italic", "serif"],
        ZodiakBold: ["Zodiak-Bold", "serif"],
        ZodiakBoldItalic: ["Zodiak-BoldItalic", "serif"],
        ZodiakExtrabold: ["Zodiak-Extrabold", "serif"],
        ZodiakExtraboldItalic: ["Zodiak-ExtraboldItalic", "serif"],
        ZodiakBlack: ["Zodiak-Black", "serif"],
        ZodiakBlackItalic: ["Zodiak-BlackItalic", "serif"],
      }
    },
  },
  plugins: [],
}
